(* Copyright (C) 2008 Vesa Karvonen
 *
 * This code is released under the MLton license, a BSD-style license.
 * See the LICENSE file or http://mlton.org/License for details.
 *)

signature CONTRACT = sig
   type 'a t
   (** The type constructor of contracts. *)

   exception Contract
   (** Raised by {pr} when the given predicate is not satisfied. *)

   exception Caller of Exn.t and Callee of Exn.t
   (**
    * The arrow combinator {-->} tags any raised exception with either
    * {Caller} or {Callee} to indicate where the blame for the contract
    * violation should be assigned.
    *)

   val assert : 'a t -> 'a UnOp.t
   (**
    * Applies the contract to the given value, returning a (possibly) new
    * value.  Higher-order values are wrapped with a contract checker that
    * checks the contract dynamically.
    *)

   val ef : 'a Effect.t -> 'a t
   (**
    * Lifts an effect to a contract.  The intention is that the effect
    * examines, but does not modify, any given value and raises an
    * exception if the value does not satisfy the desired contract.
    *)

   val pr : 'a UnPr.t -> 'a t
   (**
    * Lifts a predicate to a contract.  The contract raises {Contract} in
    * case a value does not satisfy the predicate.
    *)

   val any : 'a t
   (**
    * Contract that is satisfied by any value.  {any} is equivalent to {pr
    * (const true)}.
    *)

   val none : 'a t
   (**
    * Contract that is not satisfied by any value.  {none} is equivalent
    * to {pr (const false)}.
    *)

   val --> : 'a t * ('a -> 'b t) -> ('a -> 'b) t
   (**
    * Given a contract for the domain of type {'a} and a contract
    * constructor for the range of type {'b}, returns a contract for a
    * function of type {'a -> 'b}.
    *
    * The contract constructor for the range is given the value of the
    * domain being passed to the function.  Furthermore, it is guaranteed
    * that the contract for the range is constructed before the contracted
    * function is called.
    *)

   val andAlso : 'a t BinOp.t
   (**
    * Conjunction of contracts.  {a andAlso b} is a contract that is
    * satisfied iff both {a} and {b} are satisfied.
    *
    * To understand the interaction of {-->} and {andAlso}, consider the
    * following example:
    *
    *> fun say ms = ef (fn () => prints ms)
    *> fun con i = say ["D", i] --> (fn () => (prints ["C", i] ; say ["R", i]))
    *
    *> val () = assert (con "1" andAlso con "2") (fn () => ()) ()
    *
    * The output from the example is:
    *
    *> D2C2D1C1R1R2
    *)
end
