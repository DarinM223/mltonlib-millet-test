infix &

val _ =
  let
    fun add a b = a + b
  in
    print ("i: " ^ Int.toString (Fn.uncurry add (1, 2)) ^ "\n")
  end

type 'a person = {name: string, age: int, data: 'a}

fun person a =
  let
    open Generic
  in
    record' (R' "name" string *` R' "age" int *` R' "data" a)
      ( fn {name, age, data} => name & age & data
      , fn (name & age & data) => {name = name, age = age, data = data}
      )
  end

structure Bop =
struct
  datatype t = Add | Sub | Mul | Div

  val t =
    let
      open Generic
    in
      data' (C0' "Add" +` C0' "Sub" +` C0' "Mul" +` C0' "Div")
        (fn Div => INR () | ? => INL (case ? of
            Mul => INR () | ? => INL (case ? of
            Sub => INR () |
            Add => INL ()))
        , 
        fn INR () => Div | INL ? => case ? of
           INR () => Mul | INL ? => case ? of
           INR () => Sub | 
           INL () => Add
        )
    end
end

fun tuple5 (a, b, c, d, e) =
  let
    open Generic
  in
    tuple' (T a *` T b *` T c *` T d *` T e)
      ( fn (a, b, c, d, e) => a & b & c & d & e
      , fn a & b & c & d & e => (a, b, c, d, e)
      )
  end

structure Anf =
struct
  type var = string
  datatype value = Int of int | Var of var | Glob of var

  val value =
    let
      open Generic
    in
      data' (C1' "Int" int +` C1' "Var" string +` C1' "Glob" string)
        ( fn Int i => INL (INL i) | Var v => INL (INR v) | Glob v => INR v
        , fn INL (INL i) => Int i | INL (INR v) => Var v | INR v => Glob v
        )
    end

  datatype t =
    Halt of value
  | Fun of var * var list * t * t
  | Join of var * var option * t * t
  | Jump of var * value option
  | App of var * var * value list * t
  | Bop of var * Bop.t * value * value * t
  | If of value * t * t
  | Tuple of var * value list * t
  | Proj of var * var * int * t

  val t =
    let
      open Generic
    in
      Tie.fix Y (fn t =>
        data'
             (C1' "Halt" value +` C1' "Fun" (tuple4 (string, list string, t, t))
                +` C1' "Join" (tuple4 (string, option string, t, t)) +` C1'
                "Jump" (tuple2 (string, option value)) +` C1' "App"
                (tuple4 (string, string, list value, t)) +` C1' "Bop"
                (tuple5 (string, Bop.t, value, value, t)) +` C1' "If"
                (tuple3 (value, t, t)) +` C1' "Tuple"
                (tuple3 (string, list value, t)) +` C1' "Proj"
                (tuple4 (string, string, int, t)))
          (fn Proj ? => INR ? | ? => INL (case ? of
              Tuple ? => INR ? | ? => INL (case ? of
              If ? => INR ? | ? => INL (case ? of
              Bop ? => INR ? | ? => INL (case ? of
              App ? => INR ? | ? => INL (case ? of
              Jump ? => INR ? | ? => INL (case ? of
              Join ? => INR ? | ? => INL (case ? of
              Fun ? => INR ? |
              Halt ? => INL ?))))))),
          fn INR ? => Proj ? | INL ? => case ? of
             INR ? => Tuple ? | INL ? => case ? of
             INR ? => If ? | INL ? => case ? of
             INR ? => Bop ? | INL ? => case ? of
             INR ? => App ? | INL ? => case ? of
             INR ? => Jump ? | INL ? => case ? of
             INR ? => Join ? | INL ? => case ? of
             INR ? => Fun ? | 
             INL ? => Halt ?))
    end
end

val _ =
  let
    open Generic
  in
    print (show (list int) [1, 2, 3] ^ "\n");
    print (show (option (list int)) (SOME [1, 2, 3]) ^ "\n");
    print (Word.toString (hash (list int) [1, 2, 3]) ^ "\n");
    print (show (person int) {name = "bob", age = 25, data = 420} ^ "\n");
    print (show Anf.t (Anf.Halt (Anf.Int 1)) ^ "\n")
  end
